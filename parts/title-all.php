<?php

    if ( get_field( 'title-display' ) ) {
        $_title         = get_the_title();
        $_title_alt     = get_field( 'title-override' );
        $_title_style   = get_field( 'title-style' );
        $_subtitle      = get_field( 'title-subtitle' );

        if ( strlen( $_title_alt ) ) {
            $_title         = $_title_alt;
        }

        // Our <strong>Work</strong>
        switch ( $_title_style ) {
            case 'intro' :
                ?>
                <section class="container intro">
                    <div class="row">
                        <div class="col-md-2 intro--title">
                            <h1><?php echo dantes_heading_format( $_title ); ?></h1>
                        </div>
                        <div class="col-md-10 intro--text">
                            <p><?php echo $_subtitle; ?></p>
                        </div>
                    </div>
                </section>
                <?php
                break;

            case 'intro-alt' :
                ?>
                <section class="container intro intro-alternate">
                    <div class="row">
                        <div class="intro-content">
                            <h1><?php echo dantes_heading_format( $_title ); ?></h1>
                            <p><?php echo $_subtitle; ?></p>
                        </div>
                    </div>
                </section>
                <?php
                break;

            default :
                ?>
                <section class="container content heading">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-title"><strong><?php echo $_title; ?></strong></h1>
                        </div>
                    </div>
                </section>
                <?php
                break;
        }
    }

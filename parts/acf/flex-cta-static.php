
            <section class="container-fluid cta-image">
                <div class="row">
                    <div class="col-lg-12">
                        <img src="<?php echo wp_get_attachment_image_url( get_sub_field( 'image' ), 'cta-inline' ); ?>">
                    </div>
                </div>
            </section>

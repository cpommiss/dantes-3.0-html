<?php
    if ( have_rows( 'row' ) ) :
        ?>
        <section class="container process">
            <?php
                while ( have_rows( 'row' ) ) :
                    the_row();
                    ?>
                    <div class="row process--row">
                        <div class="col-lg-12">
                            <div class="process--row-images">
                                <?php
                                    if ( have_rows( 'images' ) ) :
                                        while ( have_rows( 'images' ) ) :
                                            the_row();

                                            $_styles            = array();
                                            $_image             = get_sub_field( 'image' );
                                            $_image_width       = intval( get_sub_field( 'image-width' ) );
                                            $_image_top         = intval( get_sub_field( 'image-top' ) );
                                            $_image_justify     = get_sub_field( 'image-justify' );
                                            $_image_left        = intval( get_sub_field( 'image-left' ) );
                                            $_image_right       = intval( get_sub_field( 'image-right' ) );
                                            $_image_opacity     = intval( get_sub_field( 'image-opacity' ) );

                                            array_push( $_styles,
                                                        ( 'top: ' . $_image_top . 'px;' ) );
                                            array_push( $_styles,
                                                        ( 'opacity: ' . ( $_image_opacity / 100 ) ) );

                                            if ( $_image_width > 0 )
                                                array_push( $_styles,
                                                            ( 'width: ' . $_image_width . '%;' ) );
                                            if ( $_image_justify === 'left' ) :
                                                array_push( $_styles,
                                                            ( 'left: ' . $_image_left . '%' ) );
                                            else :
                                                array_push( $_styles,
                                                            ( 'right: ' . $_image_right . '%' ) );
                                            endif;

                                            ?>
                                            <figure><img src="<?php echo wp_get_attachment_image_url( $_image, 'full' ); ?>" style="<?php echo implode( ' ', $_styles ); ?>"></figure>
                                            <?php
                                        endwhile;
                                    endif;
                                ?>
                            </div>
                            <?php
                                if ( have_rows( 'content' ) ) :
                                    while ( have_rows( 'content' ) ) :
                                        the_row();

                                        $_content_heading       = get_sub_field( 'heading' );
                                        $_content_data          = get_sub_field( 'content' );
                                        $_content_positioning   = array();

                                        if ( have_rows( 'position' ) ) :
                                            while ( have_rows( 'position' ) ) :
                                                the_row();

                                                $_position_top      = intval( get_sub_field( 'top' ) );
                                                $_position_left     = intval( get_sub_field( 'left' ) );
                                                $_position_right    = intval( get_sub_field( 'right' ) );

                                                if ( $_position_top > 0 ) :
                                                    array_push( $_content_positioning,
                                                                ( 'margin-top: ' . $_position_top . 'px;' ) );
                                                endif;

                                                if ( $_position_left > 0 ) :
                                                    array_push( $_content_positioning,
                                                                ( 'margin-left: ' . $_position_left . '%;' ) );
                                                endif;

                                                if ( $_position_right > 0 ) :
                                                    array_push( $_content_positioning,
                                                                'float: right;' );
                                                    array_push( $_content_positioning,
                                                                ( 'margin-right: ' . $_position_right . 'px;' ) );
                                                endif;
                                            endwhile;
                                        endif;
                                        ?>
                                        <div class="process--row-caption" style="<?php echo implode( ' ', $_content_positioning ); ?>">
                                            <h2><?php echo dantes_heading_format( $_content_heading ); ?></h2>
                                            <?php echo $_content_data; ?>
                                        </div>
                                        <?php
                                    endwhile;
                                endif;
                            ?>
                        </div>
                    </div>

                    <?php
                endwhile;
            ?>
        </section>
        <?php
    endif;
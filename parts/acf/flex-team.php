<?php
    $_display           = get_sub_field( 'type' );
    $_heading           = get_sub_field( 'heading' );
    $_content           = get_sub_field( 'content' );
    $_posts             = get_sub_field( 'team' );
    ?>
    <section class="container content">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title"><strong><?php echo $_heading; ?></strong></h2>
                <?php echo $_content; ?>
                <?php
                    if ( $_display === 'carousel' ) :
                        $carousel_id        = 'ch-carousel-team-' . uniqid();
                        ?>
                        <section class="row team-carousel">
                            <div class="col-lg-12">
                                <div id="<?php echo $carousel_id; ?>" class="carousel slide" data-ride="">
                                    <ol class="carousel-indicators">
                                        <?php
                                            $_counter    = 0;

                                            foreach ( $_posts as $post ) :
                                                setup_postdata( $post );
                                                ?>
                                                <li data-target="#<?php echo $carousel_id; ?>" data-slide-to="<?php echo $_counter; ?>"<?php echo ( ( $_counter < 2 ) ? ' class="active"' : '' ); ?>></li>
                                                <?php
                                                $_counter++;
                                            endforeach;

                                            wp_reset_postdata();
                                        ?>
                                    </ol>

                                    <div class="carousel-inner" role="listbox">
                                        <?php
                                            $_counter    = 0;

                                            foreach ( $_posts as $post ) :
                                                setup_postdata( $post );
                                                ?>
                                                <div class="item<?php echo ( ( $_counter < 2 ) ? ' active' : '' ); ?>">
                                                    <a href="javascript:;"><img src="<?php echo wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'post-carousel-small' ); ?>" alt="<?php echo get_the_title(); ?>"></a>
                                                    <div class="carousel-caption">
                                                        <h2><a href="javascript:;"><?php echo get_the_title(); ?></a></h2>
                                                        <p><?php echo substr( get_the_excerpt(), 0, 125 ); ?>...</p>
                                                    </div>
                                                </div>
                                                <?php
                                                $_counter++;
                                            endforeach;

                                            wp_reset_postdata();
                                        ?>
                                    </div>

                                    <a class="left carousel-control" href="#<?php echo $carousel_id; ?>" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#<?php echo $carousel_id; ?>" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </section>
                        <?php
                    else :
                        ?>
                        <section class="row team">
                            <?php
                                foreach ( $_posts as $post ) :
                                    setup_postdata( $post );
                                    ?>
                                    <div class="col-lg-6 team--item">
                                        <figure>
                                            <a href="javascript:;">
                                                <img src="<?php echo wp_get_attachment_image_url( get_post_thumbnail_id( $post->ID ), 'team-column-small' ); ?>">
                                            </a>

                                            <figcaption>
                                                <h2><a href="javascript:;"><?php echo get_the_title(); ?></a></h2>
                                                <p><?php echo get_the_excerpt(); ?></p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <?php
                                endforeach;

                                wp_reset_postdata();
                            ?>
                        </section>
                        <?php
                    endif;
                ?>
            </div>
        </div>
    </section>

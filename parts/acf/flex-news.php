<?php
	$_carousel_id       = 'ch-carousel-news-' . uniqid();
	$_carousel_cats		= get_sub_field( 'category' );

	if ( $_carousel_cats ) :
		$_categories		= array();

		foreach ( $_carousel_cats as $_cat ) :
			array_push(	$_categories,
						$_cat );
		endforeach;

		if ( count( $_categories ) ) :
			$_carousel_posts		=	new WP_Query(
											array(
												'category__in'		=> $_categories,
												'post_status'		=> 'publish',
												'posts_per_page'	=> intval( get_sub_field( 'count' ) )
											)
										);
			?>
			<section class="container-fluid news">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<h3><strong><?php echo get_sub_field( 'heading' ); ?></strong></h3>
							</div>
						</div>

						<div class="row">
							<div class="col-lg-12">
								<div id="<?php echo $_carousel_id; ?>" class="carousel slide" data-ride="carousel">
									<ol class="carousel-indicators">
										<?php
											$_counter	= 0;

											while ( $_carousel_posts->have_posts() ) :
												$_carousel_posts->the_post();
												?>
												<li data-target="#<?php echo $_carousel_id; ?>" data-slide-to="<?php echo $_counter; ?>"<?php echo ( ( $_counter < 1 ) ? ' class="active"' : '' ); ?>></li>
												<?php
												$_counter++;
											endwhile;

											$_carousel_posts->rewind_posts();

											wp_reset_postdata();
										?>
									</ol>

									<div class="carousel-inner" role="listbox">
										<?php
											$_counter	= 0;

											while ( $_carousel_posts->have_posts() ) :
												$_carousel_posts->the_post();

												$_title         = get_the_title();
												$_title_alt     = get_field( 'title-override', $_carousel_posts->post->ID );
												$_subtitle		= get_field( 'title-subtitle', $_carousel_posts->post->ID );

												if ( strlen( $_title_alt ) ) {
													$_title         = $_title_alt;
												}
												?>
												<div class="item<?php echo ( ( $_counter < 1 ) ? ' active' : '' ); ?>">
													<img src="<?php echo get_the_post_thumbnail_url( null, 'blog-preview' ); ?>" alt="<?php echo get_the_title(); ?>">
													<div class="carousel-caption">
														<h4>
															<?php echo dantes_heading_format( $_title ); ?>
															<?php if ( strlen( $_subtitle ) ) : ?><em><?php echo $_subtitle; ?></em><?php endif; ?>
														</h4>
														<p><?php echo get_the_excerpt(); ?></p>
														<a href="<?php echo esc_url( get_permalink() ); ?>" class="btn btn-primary">Read Full Story</a>
													</div>
												</div>
												<?php
												$_counter++;
											endwhile;

											wp_reset_postdata();
										?>
									</div>

									<a class="left carousel-control" href="#<?php echo $_carousel_id; ?>" role="button" data-slide="prev">
										<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
										<span class="sr-only"><?php _e( 'Previous', 'dantes' ); ?></span>
									</a>
									<a class="right carousel-control" href="#<?php echo $_carousel_id; ?>" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only"><?php _e( 'Next', 'dantes' ); ?></span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<?php
		endif;
	endif;
?>


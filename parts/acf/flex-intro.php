
            <section class="container intro">
                <div class="row">
                    <div class="col-md-2 intro--title">
                        <h1><?php echo dantes_heading_format( get_sub_field( 'intro-heading' ) ); ?></h1>
                    </div>
                    <div class="col-md-10 intro--text">
                        <p><?php echo get_sub_field( 'intro-content' ); ?></p>
                    </div>
                </div>
            </section>

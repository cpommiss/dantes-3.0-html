<?php

    $_cta_id        = 'ch-carousel-cta-' . uniqid();
    $_cta_inline    = get_sub_field( 'inline' );
    $_cta_slides    = array();

    if ( have_rows( 'slides' ) ) {
        while ( have_rows( 'slides' ) ) {
            the_row();

            array_push( $_cta_slides,
                        array(
                            'image'     => wp_get_attachment_image_url( get_sub_field( 'image' ), 'cta-large' ),
                            'heading'   => get_sub_field( 'heading' ),
                            'content'   => get_sub_field( 'content' ),
                            'link'      => get_sub_field( 'link' ),
                            'link-text' => get_sub_field( 'link-text' )
                        ) );
        }
    }

    if ( count( $_cta_slides ) ) :
        ?>
        <section class="container-fluid cta-carousel<?php echo ( ( $_cta_inline ) ? '-inline' : '' ); ?>">
            <div class="row">
                <div id="<?php echo $_cta_id; ?>" class="carousel slide" data-ride="carousel" data-pause="hover">
                    <ol class="carousel-indicators">
                        <?php
                            for ( $_counter = 0; $_counter < count( $_cta_slides ); $_counter++ ) {
                                ?><li data-target="#<?php echo $_cta_id; ?>" data-slide-to="<?php echo $_counter; ?>"<?php echo ( ( $_counter === 0 ) ? ' class="active"' : '' ); ?>></li><?php
                            }
                        ?>
                    </ol>

                    <div class="carousel-inner" role="listbox">
                        <?php
                            $_counter   = 0;

                            foreach ( $_cta_slides as $_slide ) {
                                ?>
                                <div class="item<?php echo ( ( $_counter === 0 ) ? ' active' : '' ); ?>">
                                    <img src="<?php echo $_slide[ 'image' ]; ?>" alt="<?php echo $_slide[ 'heading' ]; ?>">
                                    <div class="carousel-caption">
                                        <h2><?php echo dantes_heading_format( $_slide[ 'heading' ] ); ?></h2>

                                        <?php if ( strlen( $_slide[ 'content' ] ) ) : ?>
                                            <?php echo $_slide[ 'content' ]; ?>
                                        <?php endif; ?>

                                        <?php if ( strlen( $_slide[ 'link' ] ) ) : ?>
                                            <a href="<?php echo $_slide[ 'link' ]; ?>" class="btn btn-primary"><?php echo $_slide[ 'link-text' ]; ?></a>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php
                                $_counter++;
                            }
                        ?>
                    </div>

                    <a class="left carousel-control" href="#<?php echo $_cta_id; ?>" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only"><?php _e( 'Previous', 'ch' ); ?></span>
                    </a>
                    <a class="right carousel-control" href="#<?php echo $_cta_id; ?>" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only"><?php _e( 'Next', 'ch' ); ?></span>
                    </a>
                </div>
            </div>
        </section>
        <?php
    endif;
?>



            <section class="container-fluid about">
                <div class="row">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 about-intro">
                                <h3>
	                                <?php echo dantes_heading_format( get_sub_field( 'about-heading' ) ); ?>
                                </h3>
                            </div>
                            <div class="col-md-9 about-content">
	                            <?php echo get_sub_field( 'about-content' ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="container portfolio-grid">
                <div class="row">
                    <div class="col-lg-12">
	                    <?php
	                        if ( have_rows( 'grid' ) ) :
		                        ?>
	                            <ul>
                                <?php
                                    while ( have_rows( 'grid' ) ) :
	                                    the_row();

										$post	= get_sub_field( 'category' );

										if ( $post ) :
											setup_postdata( $post );

											$_styles    = array();
											$_image		= wp_get_attachment_image_url( get_sub_field( 'image' ), 'full' );
											$_width		= intval( get_sub_field( 'image-width' ) );
											$_height	= intval( get_sub_field( 'image-height' ) );
											$_top		= intval( get_sub_field( 'image-top' ) );
											$_left		= '';
											$_right		= '';

											if ( get_sub_field( 'image-justify' ) === 'left' ) {
												$_left		= intval( get_sub_field( 'image-left' ) );
											} else {
												$_right		= intval( get_sub_field( 'image-right' ) );
											}

											// dimensions
											if ( $_width > 0 ) {
												array_push( $_styles,
															'width: ' . $_width . '%;' );
											}
											if ( $_height > 0 ) {
												array_push( $_styles,
															'height: ' . $_height . '%;' );
											}

											// positioning
											array_push( $_styles,
														'top: ' . $_top . '%;' );

											if ( is_numeric( $_left ) ) {
												array_push( $_styles,
															'left: ' . $_left . '%;' );
											}
											if ( is_numeric( $_right ) ) {
												array_push( $_styles,
															'right: ' . $_right . '%;' );
											}
											?>
											<li style="background-image: url(<?php echo $_image; ?>); <?php echo implode( ' ', $_styles ); ?>">
												<a href="<?php echo get_the_permalink(); ?>">
													<strong><?php echo get_the_title(); ?></strong>
												</a>
											</li>
											<?php
											wp_reset_postdata();
										endif;
		                            endwhile;
	                            ?>
	                            </ul>
	                            <?php
	                        endif;
	                    ?>
                    </div>
                </div>
            </section>

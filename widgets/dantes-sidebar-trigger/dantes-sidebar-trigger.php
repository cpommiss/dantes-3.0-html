<?php

/*
Widget Name: DANTES - Sidebar Trigger
Description: Triggers a sidebar widget area.
Author: DANTES
*/

class DANTES_Sidebar_Trigger extends SiteOrigin_Widget {
    function __construct() {
        parent::__construct(
            'dantes-sidebar-trigger',

            __( 'DANTES - Sidebar Trigger', 'dantes' ),

            array(
                'description' => __( 'Triggers a sidebar widget area.', 'dantes' )
            ),

            array(
            ),

            array(
                'name' => array(
                    'type'      => 'text',
                    'label'     => __( 'Enter the internal name of the sidebar to trigger (TODO: replace with drop-down)', 'dantes' )
                ),
                'style' => array(
                    'type'      => 'select',
                    'label'     => __( 'Select the display style for this sidebar', 'dantes' ),
                    'default'   => 'inline',
                    'options'   => array(
                        'inline'    => __( 'Inline with content (Default)', 'dantes' ),
                        'floating'  => __( 'Floating over content', 'dantes' )
                    )
                )
            ),

            plugin_dir_path( __FILE__ )
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

    function get_template_dir($instance) {
        return 'templates';
    }
}

siteorigin_widget_register( 'dantes-sidebar-trigger', __FILE__, 'DANTES_Sidebar_Trigger' );

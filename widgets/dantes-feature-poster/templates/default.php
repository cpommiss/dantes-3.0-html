<div class="widget widget--dantes-feature-poster">
    <figure>
        <img src="<?php echo wp_get_attachment_image_url( $instance[ 'image' ], 'dantes-sidebar-feature' ); ?>" alt="<?php echo $instance[ 'title' ]; ?>" width="100%">
        <figcaption>
            <a href="#"><?php echo $instance[ 'title' ]; ?> <i class="<?php echo $instance[ 'icon' ]; ?>"></i></a>
        </figcaption>
    </figure>
</div>
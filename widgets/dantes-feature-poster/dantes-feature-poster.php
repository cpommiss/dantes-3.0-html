<?php

/*
Widget Name: DANTES - Feature Poster
Description: Creates a call-to-action poster with text and (optionally) a link to content within the site.
Author: DANTES
*/

class DANTES_Feature_Poster extends SiteOrigin_Widget {
    function __construct() {
        parent::__construct(
            'dantes-feature-poster',

            __( 'DANTES Feature Poster', 'dantes' ),

            array(
                'description' => __( 'Creates a call-to-action poster with text and (optionally) a link to content within the site.', 'dantes' )
            ),

            array(
            ),

            array(
                'image' => array(
                    'type'      => 'media',
                    'label'     => __( 'Choose an image', 'dantes' ),
                    'choose'    => __( 'Choose image', 'dantes' ),
                    'update'    => __( 'Set image', 'dantes' ),
                    'library'   => 'image',
                    'fallback'  => false
                ),
                'title' => array(
                    'type'      => 'text',
                    'label'     => __( 'The title for this call-to-action', 'dantes' ),
                    'default'   => ''
                ),
                'icon' => array(
                    'type'      => 'icon',
                    'label'     => __( 'Choose an icon to include to the right of the title', 'dantes' )
                ),
                'linkage' => array(
                    'type'      => 'link',
                    'label'     => __( 'The location to where this call-to-action should link', 'dantes' ),
                    'default'   => ''
                )
            ),

            plugin_dir_path( __FILE__ )
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

    function get_template_dir($instance) {
        return 'templates';
    }
}

siteorigin_widget_register( 'dantes-feature-poster', __FILE__, 'DANTES_Feature_Poster' );

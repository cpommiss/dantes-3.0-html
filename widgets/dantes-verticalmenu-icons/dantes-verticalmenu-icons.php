<?php

/*
Widget Name: DANTES - Vertical Menu (with icons)
Description: Creates a vertical menu with text and optional icons.
Author: DANTES
*/

class DANTES_VerticalMenu_Icons extends SiteOrigin_Widget {
    function __construct() {
        parent::__construct(
            'dantes-verticalmenu-icons',

            __( 'DANTES - Vertical Menu (with icons)', 'dantes' ),

            array(
                'description' => __( 'Creates a vertical menu with text and optional icons.', 'dantes' )
            ),

            array(
            ),

            array(
                'items' => array(
                    'type'          => 'repeater',
                    'label'         => __( 'Menu Items' , 'dantes' ),
                    'item_name'     => __( 'Menu Item', 'dantes' ),
                    'item_label'    => array(
                        'selector'     => "[id*='title']",
                        'update_event' => 'change',
                        'value_method' => 'val'
                    ),
                    'fields' => array(
                        'title' => array(
                            'type'      => 'text',
                            'label'     => __( 'Enter the title for this menu item', 'dantes' )
                        ),
                        'icon' => array(
                            'type'      => 'icon',
                            'label'     => __( 'Choose an icon to include to the right of the title', 'dantes' )
                        ),
                        'linkage' => array(
                            'type'      => 'link',
                            'label'     => __( 'The location to where this menu item should link', 'dantes' ),
                            'default'   => ''
                        ),
                        'style' => array(
                            'type'      => 'select',
                            'label'     => __( 'Choose the style of this menu item.', 'dantes' ),
                            'default'   => 'tall',
                            'options'   => array(
                                'tall'      => __( 'Tall (Default)', 'dantes' ),
                                'short'     => __( 'Short', 'dantes' )
                            )
                        ),
                        'bgcolor' => array(
                            'type'      => 'color',
                            'label'     => __( 'Choose the background color for this menu item', 'dantes' ),
                            'default'   => '#2e336d'
                        )
                    )
                )
            ),

            plugin_dir_path( __FILE__ )
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

    function get_template_dir($instance) {
        return 'templates';
    }
}

siteorigin_widget_register( 'dantes-verticalmenu-icons', __FILE__, 'DANTES_VerticalMenu_Icons' );

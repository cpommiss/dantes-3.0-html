<?php
    if ( ( isset( $instance[ 'items' ] ) ) && ( is_array( $instance[ 'items' ] ) ) && ( count( $instance[ 'items' ] ) ) ) :
        ?>
        <aside class="widget widget--dantes-vertical-menu">
            <menu>
                <ul>
                    <?php
                        foreach ( $instance[ 'items' ] as $item ) :
                            ?>
                            <li class="style--<?php echo $item[ 'style' ]; ?>" style="background-color: <?php echo $item[ 'bgcolor' ]; ?>">
                                <a href="#" class="<?php echo $item[ 'icon' ]; ?>"><?php echo $item[ 'title' ]; ?></a>
                            </li>
                            <?php
                        endforeach;
                    ?>
                </ul>
            </menu>
        </aside>
        <?php
    endif;
?>

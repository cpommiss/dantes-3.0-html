<?php

/*
Widget Name: DANTES - Feature Link
Description: Creates a call-to-action link with an image and heading.
Author: DANTES
*/

class DANTES_Feature_Link extends SiteOrigin_Widget {
    function __construct() {
        parent::__construct(
            'dantes-feature-link',

            __( 'DANTES Feature Link', 'dantes' ),

            array(
                'description' => __( 'Creates a call-to-action link with an image and heading.', 'dantes' )
            ),

            array(
            ),

            array(
                'image' => array(
                    'type'      => 'media',
                    'label'     => __( 'Choose an image', 'dantes' ),
                    'choose'    => __( 'Choose image', 'dantes' ),
                    'update'    => __( 'Set image', 'dantes' ),
                    'library'   => 'image',
                    'fallback'  => false
                ),
                'title' => array(
                    'type'      => 'text',
                    'label'     => __( 'The title for this call-to-action', 'dantes' ),
                    'default'   => ''
                ),
                'linkage' => array(
                    'type'      => 'link',
                    'label'     => __( 'The location to where this call-to-action should link', 'dantes' ),
                    'default'   => ''
                )
            ),

            plugin_dir_path( __FILE__ )
        );
    }

    function get_template_name($instance) {
        return 'default';
    }

    function get_template_dir($instance) {
        return 'templates';
    }
}

siteorigin_widget_register( 'dantes-feature-link', __FILE__, 'DANTES_Feature_Link' );

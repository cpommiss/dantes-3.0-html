<div class="widget widget--feature-link">
    <figure>
        <a href="#">
            <figcaption><?php echo $instance[ 'title' ]; ?></figcaption>

            <img src="<?php echo wp_get_attachment_image_url( $instance[ 'image' ], 'dantes-feature-link' ); ?>" alt="<?php echo $instance[ 'title' ]; ?>">
        </a>
    </figure>
</div>
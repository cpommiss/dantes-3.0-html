<?php
	get_header();
	?>
	<section class="container flex-content">
		<div class="row">
			<article class="col-md-8">
			<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						get_template_part( 'parts/content', 'page' );
					endwhile;
				else :
					get_template_part( 'content', 'none' );
				endif;
			?>
			</article>
			<aside class="col-md-4 flex-content--sidebar">
				<?php get_sidebar(); ?>
			</aside>
		</div>
	</section>
	<?php
	get_footer();
?>

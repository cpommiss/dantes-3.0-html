<!DOCTYPE html>
<html lang="en">
    <head>
	    <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

	    <title><?php wp_title( '|', true, 'right' ); ?></title>

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

		<?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <header class="dantes-header" id="dantes-header" role="navigation">
	        <section class="dantes-header--inner" id="dantes-header--inner">
		        <nav class="dantes-header--nav container" id="dantes-header--nav">
			        <div class="dantes-header--logo">
				        <a href="<?php echo bloginfo( 'url' ); ?>/">
					        <strong><?php echo bloginfo( 'name' ); ?></strong>
				        </a>
			        </div>

			        <ul role="menubar">
			            <li role="presentation">
				            <a href="/examinations/index.html" role="menuitem" aria-haspopup="true">
					            <strong>DANTES Examinations</strong>
				            </a>

				            <menu class="dantes-header--nav__submenu">
					            <div class="dantes-header--nav__submenu-container container">
						            <ul role="menu">
							            <li role="presentation"><a href="/examinations/examinations-guide.html" role="menuitem">Examinations Guide</a></li>
							            <li role="presentation">
								            Funding &amp; Reimbursement Eligibility

								            <ul role="menu">
									            <li role="presentation"><a href="/examinations/funding-and-reimbursement-eligibility/funding-eligibility.html" role="menuitem">Funding Eligibility</a></li>
									            <li role="presentation"><a href="/examinations/funding-and-reimbursement-eligibility/reimbursement-eligibility.html" role="menuitem">Reimbursement Eligibility</a></li>
								            </ul>
							            </li>
							            <li role="presentation">
								            Earn College Credit

								            <ul role="menu">
									            <li role="presentation"><a href="/examinations/earn-college-credit/earn-college-credit.html" role="menuitem">Earn College Credit Guide</a></li>
									            <li role="presentation"><a href="/examinations/earn-college-credit/clep.html" role="menuitem">CLEP</a></li>
									            <li role="presentation"><a href="/examinations/earn-college-credit/dsst.html" role="menuitem">DSST</a></li>
									            <li role="presentation"><a href="/examinations/earn-college-credit/pass-rates.html" role="menuitem">Pass Rates</a></li>
								            </ul>
							            </li>
						            </ul>
						            <ul role="menu">
							            <li role="presentation">
								            College Admissions

								            <ul role="menu">
									            <li role="presentation"><a href="/examinations/college-admissions/college-admissions-guide.html" role="menuitem">College Admissions Guide</a></li>
									            <li role="presentation"><a href="/examinations/college-admissions/act.html" role="menuitem">ACT</a></li>
									            <li role="presentation"><a href="/examinations/college-admissions/sat.html" role="menuitem">SAT</a></li>
									            <li role="presentation"><a href="/examinations/college-admissions/gmat.html" role="menuitem">GMAT</a></li>
									            <li role="presentation"><a href="/examinations/college-admissions/gre.html" role="menuitem">GRE</a></li>
									            <li role="presentation"><a href="/examinations/college-admissions/lsat.html" role="menuitem">LSAT</a></li>
								            </ul>
							            </li>
							            <li role="presentation"><a href="/examinations/transcripts.html" role="menuitem">Transcripts</a></li>
							            <li role="presentation"><a href="/examinations/test-control-officer.html" role="menuitem">Test Control Officer</a></li>
						            </ul>
						            <ul role="menu">
							            <li role="presentation">
								            High School Equivalency

								            <ul role="menu">
									            <li role="presentation"><a href="/examinations/high-school-equivalency/high-school-equivalency-guide.html" role="menuitem">High School Equivalency Guide</a></li>
									            <li role="presentation"><a href="/examinations/high-school-equivalency/ged.html" role="menuitem">GED</a></li>
								            </ul>
							            </li>
							            <li role="presentation">
								            Teacher Certification

								            <ul role="menu">
									            <li role="presentation"><a href="/examinations/teacher-certification/teacher-certification-guide.html" role="menuitem">Teacher Certification Guide</a></li>
									            <li role="presentation"><a href="/examinations/teacher-certification/praxis.html" role="menuitem">PRAXIS</a></li>
								            </ul>
							            </li>
						            </ul>
					            </div>
				            </menu>
			            </li>
			            <li role="presentation">
				            <a href="/service-members/index.html" role="menuitem" aria-haspopup="true">
					            <strong>Service Members</strong>
				            </a>

				            <menu class="dantes-header--nav__submenu">
					            <div class="dantes-header--nav__submenu-container container">
						            <ul role="menu">
							            <li role="presentation">
								            Voluntary Education Sites

								            <ul role="menu">
									            <li role="presentation"><a href="/service-members/voluntary-education/army.html" role="menuitem">Army</a></li>
									            <li role="presentation"><a href="/service-members/voluntary-education/navy.html" role="menuitem">Navy</a></li>
									            <li role="presentation"><a href="/service-members/voluntary-education/marines.html" role="menuitem">Marines</a></li>
									            <li role="presentation"><a href="/service-members/voluntary-education/air-force.html" role="menuitem">Air Force</a></li>
									            <li role="presentation"><a href="/service-members/voluntary-education/coast-guard.html" role="menuitem">Coast Guard</a></li>
								            </ul>
							            </li>
							            <li role="presentation">
								            Prep for College

								            <ul role="menu">
									            <li role="presentation"><a href="/service-members/prep-for-college/prep-for-college-guide.html" role="menuitem">Prep for College Guide</a></li>
									            <li role="presentation"><a href="/service-members/prep-for-college/kuder.html" role="menuitem">Kuder® Journey</a></li>
									            <li role="presentation"><a href="/service-members/prep-for-college/oasc-cpst.html" role="menuitem">OASC - CPST</a></li>
									            <li role="presentation"><a href="/service-members/prep-for-college/dlrsa.html" role="menuitem">DLRSA</a></li>
									            <li role="presentation"><a href="/service-members/prep-for-college/tutor.html" role="menuitem">Tutor.com</a></li>
								            </ul>
							            </li>
						            </ul>
						            <ul role="menu">
							            <li role="presentation">
								            Choose a School

								            <ul role="menu">
									            <li role="presentation"><a href="/service-members/choose-a-school/choose-a-soc-dns-school.html" role="menuitem">Choose a SOC DNS School</a></li>
									            <li role="presentation"><a href="/service-members/choose-a-school/choose-a-dod-mou-school.html" role="menuitem">Choose a DoD MOU School</a></li>
									            <li role="presentation"><a href="/service-members/choose-a-school/ta-decide.html" role="menuitem">TA DECIDE</a></li>
								            </ul>
							            </li>
							            <li role="presentation"><a href="/service-members/federal-financial-aid-scholarships.html" role="menuitem" ata-link-type="local">Federal Financial Aid &amp; Scholarships</a></li>
							            <li role="presentation"><a href="/service-members/what-you-need-to-know-about-student-loans.html" role="menuitem">What you need to know about student loans</a></li>
							            <li role="presentation"><a href="/service-members/va-education-benefits.html" role="menuitem">VA Education Benefits</a></li>
						            </ul>
						            <ul role="menu">
							            <li role="presentation">
								            College Admissions

								            <ul role="menu">
									            <li role="presentation"><a href="/service-members/college-entrance-admissions/examinations.html" role="menuitem">Entrance Examinations</a></li>
									            <li role="presentation"><a href="/service-members/college-entrance-admissions/student-enrollment-procedures.html" role="menuitem">Student Enrollment Procedures</a></li>
								            </ul>
							            </li>
							            <li role="presentation">
								            Alternatives to Traditional College Credit

								            <ul role="menu">
									            <li role="presentation"><a href="/service-members/ta-alternatives/earn-college-credit-guide.html" role="menuitem">Earn College Credit Guide</a></li>
									            <li role="presentation"><a href="/service-members/ta-alternatives/jst-recommended-college-credit.html" role="menuitem">Joint Services Transcript</a></li>
									            <li role="presentation"><a href="/service-members/ta-alternatives/clep-dsst.html" role="menuitem">CLEP &amp; DSST</a></li>
									            <li role="presentation"><a href="/service-members/ta-alternatives/tips-for-successful-completion.html" role="menuitem">Tips for Successful Completion</a></li>
								            </ul>
							            </li>
						            </ul>
						            <ul role="menu">
							            <li role="presentation"><a href="/service-members/state-tuition-policies.html" role="menuitem">State Tuition Policies</a></li>
							            <li role="presentation"><a href="/service-members/financial-assistance.html" role="menuitem">Financial Assistance</a></li>
							            <li role="presentation">
								            Become a Teacher

								            <ul role="menu">
									            <li><a href="/service-members/become-a-teacher/troops-to-teachers.html" role="menuitem">Troops to Teachers</a></li>
									            <li><a href="/service-members/become-a-teacher/praxis.html" role="menuitem">PRAXIS</a></li>
								            </ul>
							            </li>
							            <li role="presentation"><a href="/education-counselors/education-events.html" role="menuitem">Education Events</a></li>
						            </ul>
					            </div>
				            </menu>
			            </li>
			            <li role="presentation">
				            <a href="/educational-institutions/index.html" role="menuitem" aria-haspopup="true">
					            <strong>Educational Institutions</strong>
				            </a>

				            <menu class="dantes-header--nav__submenu">
					            <div class="dantes-header--nav__submenu-container container">
						            <ul role="menu">
							            <li role="presentation"><a href="/service-members/choose-a-school/ta-decide.html" role="menuitem">TA DECIDE</a></li>
							            <li role="presentation"><a href="/educational-institutions/dod-mou.html" role="menuitem">DoD MOU</a></li>
							            <li role="presentation"><a href="/service-members/choose-a-school/choose-a-soc-dns-school.html" role="menuitem">Servicemembers Opportunity Colleges</a></li>
						            </ul>
					            </div>
				            </menu>
			            </li>
			            <li role="presentation">
				            <a href="/education-counselors/index.html" role="menuitem" aria-haspopup="true">
					            <strong>Education Counselors</strong>
				            </a>

				            <menu class="dantes-header--nav__submenu">
					            <div class="dantes-header--nav__submenu-container container">
						            <ul role="menu">
							            <li role="presentation"><a href="/education-counselors/education-events.html" role="menuitem">Education Events</a></li>
							            <li role="presentation"><a href="/service-members/choose-a-school/choose-a-soc-dns-school.html" role="menuitem">Servicemembers Opportunity Colleges</a></li>
						            </ul>
						            <ul role="menu">
							            <li role="presentation">
								            Education Counselor's Toolbox

								            <ul role="menu">
									            <li role="presentation"><a href="/education-counselors/education-counselor-toolbox/counselor-toolbox.html" role="menuitem">Counselor's Toolbox</a></li>
									            <li role="presentation"><a href="/education-counselors/education-counselor-toolbox/support-programs-for-service-members.html" role="menuitem">Support Programs for Service Members</a></li>
								            </ul>
							            </li>
						            </ul>
					            </div>
				            </menu>
			            </li>
			            <li role="presentation">
				            <a href="/troops-to-teachers/troops-to-teachers.html" role="menuitem" aria-haspopup="true">
					            <strong>Troops to Teachers</strong>
				            </a>

				            <menu class="dantes-header--nav__submenu">
					            <div class="dantes-header--nav__submenu-container container">
						            <ul role="menu">
							            <li role="presentation"><a href="http://www.proudtoserveagain.com" role="menuitem" target="_blank" data-scout-event="ExternalLink, Click, www.proudtoserveagain.com">Troops to Teachers Website</a></li>
							            <li role="presentation"><a href="/troops-to-teachers/troops-to-teachers.html" role="menuitem">Learn More About Troops To Teachers</a></li>
							            <li role="presentation"><a href="/examinations/teacher-certification/teacher-certification-guide.html" role="menuitem">Teacher Certification Guide</a></li>
						            </ul>
					            </div>
				            </menu>
			            </li>
			            <li role="presentation">
				            <a href="http://dantes4military.education/" role="menuitem" target="_blank">
					            <strong>Blog</strong>
				            </a>
			            </li>
			            <li class="dantes-header--nav__search" role="presentation">
				            <a href="/search.html" role="menuitem">
					            <strong>Search</strong>
				            </a>

				            <aside class="dantes-header--nav__submenu-search">
					            <form action="/search.html" method="get" class="dantes-header--search">
						            <div class="dantes-header--search__controls">
							            <label>
								            <strong>Search the DANTES Website</strong>
								            <input type="text" name="search" id="dantes-header--search">
							            </label>

							            <button type="submit">Submit</button>
							            <button type="reset">Clear</button>
						            </div>
					            </form>
				            </aside>
			            </li>
		            </ul>

			        <a href="#" class="dantes-header--nav__control"><?php _e( 'Return to Top', 'dantes' ); ?></a>
		        </nav>
	        </section>
        </header>

        <main>

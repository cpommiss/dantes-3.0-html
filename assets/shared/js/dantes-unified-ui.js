jQuery ( document ).ready( function() {
	var host_name	= new RegExp(location.host);
	var dantes_url  = 'http://www.dantes.doded.mil';
	var data_cache;
	var options = {
		"element_header":   ( ( typeof DANTES_MENU__ELEMENT_HEADER === 'string' ) ? DANTES_MENU__ELEMENT_HEADER : '' ),
		"element_footer":   ( ( typeof DANTES_MENU__ELEMENT_FOOTER === 'string' ) ? DANTES_MENU__ELEMENT_FOOTER : '' ),
		"prepend_url":      ( ( typeof DANTES_MENU__PREPEND_URL === 'string' ) ? DANTES_MENU__PREPEND_URL.replace( /\/$/, '' ) : dantes_url ),
		"prepend_link_url": ( ( typeof DANTES_MENU__PREPEND_LINK_URL === 'string' ) ? DANTES_MENU__PREPEND_LINK_URL.replace( /\/$/, '' ) : dantes_url ),
		"auto_stylesheet":  ( ( typeof DANTES_MENU__AUTO_STYLESHEET === 'boolean' ) ? DANTES_MENU__AUTO_STYLESHEET : false ),
		"search_overlay":   ( ( typeof DANTES_MENU__SEARCH_OVERLAY === 'boolean' ) ? DANTES_MENU__SEARCH_OVERLAY : true )
	};

	if ( ( jQuery( 'header#dantes-header' ).length === 0 ) && ( jQuery( 'footer#dantes-footer' ).length === 0 ) ) {
		jQuery.get( options.prepend_url + '/index.html',
					function( data ) {
						if ( (typeof data === 'string') && (data.length) ) {
							data_cache      = data;

							var navigation  = jQuery.parseHTML( jQuery.trim( data ), document, false );

							// regex to match URI's without a scheme/protocol
							var url_match = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;

							if ( navigation.length ) {
								if ( options.auto_stylesheet ) {
									// add stylesheet to document
									jQuery( 'head' ).append( '<link href="' + options.prepend_url + '/_common/css/dantes-unified-ui.css?' + Math.random() + '" rel="stylesheet" type="text/css" media="screen"/>' );
								}

								/*
								 * unfortunately, running the HTML through jQuery to find the correct elements to copy is just very troublesome, as it seems to destroy certain elements and
								 * reformat code completely, which is not going to work out for this solution. so, we traverse the tree ourselves, based on the assumption that the core
								 * structure of the source site will not change without the opportunity to change the code below at the same time.
								 */
								for ( var counter = 0; counter < navigation.length; counter++) {
									if ( ( navigation[ counter ].id === 'container' ) ) {
										var navigation_children     = navigation[ counter ].childNodes;

										for ( var element = 0; element < navigation_children.length; element++ ) {
											if ( ( navigation_children[ element ].nodeName.toLowerCase() === 'header' ) && ( navigation_children[ element ].id === 'dantes-header' ) ) {
												// add HTML to document
												var target      = ( options.element_header.length ) ? jQuery( options.element_header ) : jQuery( 'body' );

												if ( options.element_header.length ) {
													target.replaceWith( navigation_children[ element ].outerHTML );
												} else {
													target.prepend( navigation_children[ element ].outerHTML );
												}
											} else if ( ( navigation_children[ element ].nodeName.toLowerCase() === 'footer' ) && ( navigation_children[ element ].id === 'dantes-footer' ) ) {
												// add HTML to document
												var target      = ( options.element_footer.length ) ? jQuery( options.element_footer ) : jQuery( 'body' );

												if ( options.element_footer.length ) {
													target.replaceWith( navigation_children[ element ].outerHTML );
												} else {
													target.append( navigation_children[ element ].outerHTML );
												}
											}
										}
									}
								}

								jQuery( 'header#dantes-header [href], header#dantes-header [action], footer#dantes-footer [href], footer#dantes-footer [action]' ).each( function() {
									var current     = jQuery( this );
									var current_uri = current.attr( 'href' ) || jQuery( this ).attr( 'action' );

									if ( (typeof current_uri === 'string') && (!url_match.test( current_uri )) ) {
										// we need to prepend a URL to this href/action since it has no scheme/protocol
										switch ( true ) {
											case (current.is( '[href]' )) :
												current.attr( 'href', options.prepend_link_url + current_uri );
												break;

											case (current.is( '[action]' )) :
												current.attr( 'action', options.prepend_link_url + current_uri );
												break;
										}
									}
								} );

								jQuery( 'header#dantes-header nav > ul>li>menu .dantes-header--nav__submenu-container' ).each( function() {
									if ( jQuery( '>ul', this ).length === 1 ) {
										jQuery( this ).addClass( 'only-child' );
									}
								} );

								// bind events
								_bind_events();
							}
						}
					},
					'text' );
	} else {
		_bind_events();
	}
	
	function _bind_events() {
		// bind menu events
		jQuery( '.dantes-header--nav__control' ).on( 'click', function( e ) {
			e.preventDefault();

			jQuery( 'html' ).toggleClass( 'dantes-header--open' );

			// optionally close all menu options
			//jQuery( '.dantes-header--nav li.open' ).removeClass( 'open' );
		} );
		jQuery( '.dantes-header--nav ul li' ).each( function() {
			if ( ( jQuery( this ).children( '.dantes-header--nav__submenu' ).length ) || ( jQuery( this ).children( 'ul' ).length ) ) {
				jQuery( this ).addClass( 'has-children' ).prepend( '<span class="dantes-header--nav__submenu-drilldown"></span>' );
			}
		} );
		jQuery( '.dantes-header--nav__submenu-drilldown' ).on( 'click', function( e ) {
			e.preventDefault();
			e.stopPropagation();

			jQuery( this ).parent().toggleClass( 'open' );
		} );
		jQuery( '.dantes-footer--sitemap-trigger' ).on( 'click', function( e ) {
			e.preventDefault();

			var sitemap         = jQuery( '#dantes-footer' );

			if ( sitemap.hasClass( 'open' ) ) {
				sitemap.removeClass( 'open' );
			} else {
				sitemap.addClass( 'open' );

				setTimeout( function() {
								jQuery.scrollTo(    'footer#dantes-footer',
													{
														"duration": 500,
														"offset": -( jQuery( 'body' ).position().top + parseFloat( jQuery( 'body' ).css( 'marginTop' ).replace( /[^-\d\.]/g, '' ) ) )
													} );
							}, 250 );
			}
		} );

		// bind search events
		if ( options.search_overlay ) {
			jQuery( '.dantes-header--nav__search' ).append( jQuery( '#dantes-header--search__template' ).html() );
			jQuery( '.dantes-header--nav__search>a' ).on( 'click', function( e ) {
				e.preventDefault();

				var parent          = jQuery( this ).parent();
				var doc             = jQuery( 'html' );

				if ( doc.hasClass( 'dantes-header--search-open' ) ) {
					doc.removeClass( 'dantes-header--search-open' );
				} else {
					doc.addClass( 'dantes-header--search-open' );

					setTimeout( function() {
						parent.find( 'input[type=text]' )[0].focus();
					}, 350 );
				}
			} );
			jQuery( '.dantes-header--nav__search button[type="reset"]' ).on( 'click', function( e ) {
				e.preventDefault();

				jQuery( 'html' ).removeClass( 'dantes-header--search-open' );
			} );
		}
		
		// bind link trapping
		_find_external_links();
	}
	
	function _find_external_links() {
		jQuery( 'a' ).each( function() {
			var url		= jQuery( this ).attr( 'href' );

			if ( ( url ) && ( url.length ) ) {
				if ( ( url.indexOf( host_name ) == -1 ) && ( url.indexOf( 'dantes.doded.mil' ) == -1 ) ) {
					if ( ( url.slice( 0, 1 ) == '#' ) || ( url.indexOf( 'http' ) == -1 ) ) {
						jQuery( this ).attr( 'data-link-type', 'anchor' );
					} else if ( ( host_name.test( url ) ) || ( url.indexOf( 'http' ) == -1 ) || ( jQuery( this ).hasClass( 'fresco' ) ) ) {
						jQuery( this ).attr( 'data-link-type', 'local' );
					} else {
						jQuery( this ).attr( 'data-link-type', 'external' );
						jQuery( this ).attr( 'target', '_blank' );
					}
				} else {
					jQuery( this ).attr( 'data-link-type', 'local' );
				}
			}
		});

		jQuery( 'a[data-link-type="external"]' ).bind( 'click', _trap_external_link );
		jQuery( 'a[data-document-warning="true"]' ).bind('click', _trap_document_with_warning );
	}
	
	function _trap_external_link( e ) {
		var url             = jQuery( this ).attr( 'href' );

		if ( ( url ) && ( url.length ) ) {
			if ( confirm( 'PLEASE NOTE!\n\nYou are about to leave the DANTES website and access an external website.\n\nThe appearance of hyperlinks does not constitute endorsement by the (Department of Defense/the U.S. Army/the U.S. Navy/the U.S. Air Force/the U.S. Marine Corps, etc.) of this Web site or the information, products or services contained therein. For other than authorized activities such as military exchanges and Morale, Welfare and Recreation sites, the (Department of Defense/the U.S. Army/the U.S. Navy/the U.S. Air Force/the U.S. Marine Corps, etc.) does not exercise any editorial control over the information you may find at these locations. Such links are provided consistent with the stated purpose of this DoD Web site.\n\nPress [OK] to continue to the requested website, or [CANCEL] to return to the DANTES website.' ) ) {
				return true;
			} else {
				e.preventDefault();
				return false;
			}
		}
	}
	
	function _trap_document_with_warning( e ) {
		if ( confirm( 'PLEASE NOTE!\n\nThe document you are about to view on the DANTES website may contain links to external sites. If so, DANTES is not responsible and does not endorse the information or links you may find at the external site.\n\nPress [OK] to continue to the requested document, or [CANCEL] to return to the DANTES website.' ) ) {
			return true;
		} else {
			e.preventDefault();
			return false;
		}
	}
} );

/*!
 * jQuery.scrollTo
 * Copyright (c) 2007-2015 Ariel Flesler - aflesler ? gmail � com | http://flesler.blogspot.com
 * Licensed under MIT
 * http://flesler.blogspot.com/2007/10/jqueryscrollto.html
 * @projectDescription Lightweight, cross-browser and highly customizable animated scrolling with jQuery
 * @author Ariel Flesler
 * @version 2.1.2
 */
;(function(factory) {
	'use strict';
	if (typeof define === 'function' && define.amd) {
		// AMD
		define(['jquery'], factory);
	} else if (typeof module !== 'undefined' && module.exports) {
		// CommonJS
		module.exports = factory(require('jquery'));
	} else {
		// Global
		factory(jQuery);
	}
})(function($) {
	'use strict';

	var $scrollTo = $.scrollTo = function(target, duration, settings) {
		return $(window).scrollTo(target, duration, settings);
	};

	$scrollTo.defaults = {
		axis:'xy',
		duration: 0,
		limit:true
	};

	function isWin(elem) {
		return !elem.nodeName ||
			$.inArray(elem.nodeName.toLowerCase(), ['iframe','#document','html','body']) !== -1;
	}

	$.fn.scrollTo = function(target, duration, settings) {
		if (typeof duration === 'object') {
			settings = duration;
			duration = 0;
		}
		if (typeof settings === 'function') {
			settings = { onAfter:settings };
		}
		if (target === 'max') {
			target = 9e9;
		}

		settings = $.extend({}, $scrollTo.defaults, settings);
		// Speed is still recognized for backwards compatibility
		duration = duration || settings.duration;
		// Make sure the settings are given right
		var queue = settings.queue && settings.axis.length > 1;
		if (queue) {
			// Let's keep the overall duration
			duration /= 2;
		}
		settings.offset = both(settings.offset);
		settings.over = both(settings.over);

		return this.each(function() {
			// Null target yields nothing, just like jQuery does
			if (target === null) return;

			var win = isWin(this),
				elem = win ? this.contentWindow || window : this,
				$elem = $(elem),
				targ = target,
				attr = {},
				toff;

			switch (typeof targ) {
				// A number will pass the regex
				case 'number':
				case 'string':
					if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)) {
						targ = both(targ);
						// We are done
						break;
					}
					// Relative/Absolute selector
					targ = win ? $(targ) : $(targ, elem);
					/* falls through */
				case 'object':
					if (targ.length === 0) return;
					// DOMElement / jQuery
					if (targ.is || targ.style) {
						// Get the real position of the target
						toff = (targ = $(targ)).offset();
					}
			}

			var offset = $.isFunction(settings.offset) && settings.offset(elem, targ) || settings.offset;

			$.each(settings.axis.split(''), function(i, axis) {
				var Pos	= axis === 'x' ? 'Left' : 'Top',
					pos = Pos.toLowerCase(),
					key = 'scroll' + Pos,
					prev = $elem[key](),
					max = $scrollTo.max(elem, axis);

				if (toff) {// jQuery / DOMElement
					attr[key] = toff[pos] + (win ? 0 : prev - $elem.offset()[pos]);

					// If it's a dom element, reduce the margin
					if (settings.margin) {
						attr[key] -= parseInt(targ.css('margin'+Pos), 10) || 0;
						attr[key] -= parseInt(targ.css('border'+Pos+'Width'), 10) || 0;
					}

					attr[key] += offset[pos] || 0;

					if (settings.over[pos]) {
						// Scroll to a fraction of its width/height
						attr[key] += targ[axis === 'x'?'width':'height']() * settings.over[pos];
					}
				} else {
					var val = targ[pos];
					// Handle percentage values
					attr[key] = val.slice && val.slice(-1) === '%' ?
						parseFloat(val) / 100 * max
						: val;
				}

				// Number or 'number'
				if (settings.limit && /^\d+$/.test(attr[key])) {
					// Check the limits
					attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max);
				}

				// Don't waste time animating, if there's no need.
				if (!i && settings.axis.length > 1) {
					if (prev === attr[key]) {
						// No animation needed
						attr = {};
					} else if (queue) {
						// Intermediate animation
						animate(settings.onAfterFirst);
						// Don't animate this axis again in the next iteration.
						attr = {};
					}
				}
			});

			animate(settings.onAfter);

			function animate(callback) {
				var opts = $.extend({}, settings, {
					// The queue setting conflicts with animate()
					// Force it to always be true
					queue: true,
					duration: duration,
					complete: callback && function() {
						callback.call(elem, targ, settings);
					}
				});
				$elem.animate(attr, opts);
			}
		});
	};

	// Max scrolling position, works on quirks mode
	// It only fails (not too badly) on IE, quirks mode.
	$scrollTo.max = function(elem, axis) {
		var Dim = axis === 'x' ? 'Width' : 'Height',
			scroll = 'scroll'+Dim;

		if (!isWin(elem))
			return elem[scroll] - $(elem)[Dim.toLowerCase()]();

		var size = 'client' + Dim,
			doc = elem.ownerDocument || elem.document,
			html = doc.documentElement,
			body = doc.body;

		return Math.max(html[scroll], body[scroll]) - Math.min(html[size], body[size]);
	};

	function both(val) {
		return $.isFunction(val) || $.isPlainObject(val) ? val : { top:val, left:val };
	}

	// Add special hooks so that window scroll properties can be animated
	$.Tween.propHooks.scrollLeft =
	$.Tween.propHooks.scrollTop = {
		get: function(t) {
			return $(t.elem)[t.prop]();
		},
		set: function(t) {
			var curr = this.get(t);
			// If interrupt is true and user scrolled, stop animating
			if (t.options.interrupt && t._last && t._last !== curr) {
				return $(t.elem).stop();
			}
			var next = Math.round(t.now);
			// Don't waste CPU
			// Browsers don't render floating point scroll
			if (curr !== next) {
				$(t.elem)[t.prop](next);
				t._last = this.get(t);
			}
		}
	};

	// AMD requirement
	return $scrollTo;
});
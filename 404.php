<?php get_header(); ?>

            <section class="container-fluid core-heading">
                <header class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1><?php _e( '404 Not Found', 'dantes' ); ?></h1>
                        </div>
                    </div>
                </header>
            </section>

            <nav class="container core-breadcrumbs">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li class="selected"><a href=".">Page Not Found</a></li>
                </ul>
            </nav>

            <section class="container flex-content">
                <div class="row">
                    <article class="col-lg-12">
                        <p><?php _e( 'It looks like nothing was found at this location. Sorry about that!', 'dantes' ); ?></p>
                    </article>
                </div>
            </section>

<?php get_footer(); ?>
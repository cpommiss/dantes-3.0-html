<?php
if ( ! function_exists( 'dantes_setup' ) ) :

	function dantes_setup() {
		load_theme_textdomain( 'dantes', get_template_directory() . '/languages' );

		add_editor_style( array( 'css/editor.css' ) );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 800, 600, true );

		add_image_size( 'dantes-sidebar-feature', ( 360 * 2 ), ( 410 * 2 ), true );
		add_image_size( 'dantes-feature-link', ( 371 * 2 ), ( 223 * 2 ), true );

		register_nav_menus( array(
			'nav-main'		=> __( 'Header Navigation Menu', 'dantes' ),
			'nav-footer'	=> __( 'Footer Navigation Menu', 'dantes' ),
			'nav-social'	=> __( 'Social Media Menu', 'dantes' )
		) );

		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );

		add_filter( 'use_default_gallery_style', '__return_false' );

		add_theme_support( 'customize-selective-refresh-widgets' );

		show_admin_bar( false );
	}

endif;
add_action( 'after_setup_theme', 'dantes_setup' );


function dantes_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Primary Sidebar', 'dantes' ),
		'id'            => 'sidebar-default',
		'description'   => __( 'Main sidebar that appears on the left.', 'dantes' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'dantes_widgets_init' );


function dantes_assets() {
	wp_enqueue_style( 'dantes-style-bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap-sass/assets/stylesheets/bootstrap.css' );
	wp_enqueue_style( 'dantes-style', get_template_directory_uri() . '/assets/css/theme.css' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'dantes-bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap-sass/assets/javascripts/bootstrap.min.js', array( 'jquery' ), '3.3.6', true );
	//wp_enqueue_script( 'dantes-framework', get_template_directory_uri() . '/assets/js/framework.js', array( 'jquery', 'dantes-bootstrap' ), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'dantes_assets' );


function dantes_assets_admin() {
	wp_enqueue_style( 'dantes-style-admin', get_template_directory_uri() . '/assets/css/theme-admin.css' );
}
add_action( 'admin_enqueue_scripts', 'dantes_assets_admin' );


function dantes_body_classes( $classes ) {
	if ( is_singular() && ! is_front_page() ) {
		$classes[] = 'singular';
	}
	if ( is_front_page() ) {
		$classes[] = 'home';
	} else {
		$classes[] = 'interior';
	}

	return $classes;
}
add_filter( 'body_class', 'dantes_body_classes' );


function dantes_post_classes( $classes ) {
	if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
		$classes[] = 'has-post-thumbnail';
	}

	return $classes;
}
add_filter( 'post_class', 'dantes_post_classes' );


function dantes_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'dantes' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'dantes_wp_title', 10, 2 );


function dantes_heading_format( $title ) {
	if ( strpos( $title, '|' ) !== false ) {
		$parts	= explode( '|', $title );

		if ( count( $parts ) >= 2 ) {
			$title		= array_shift( $parts ) . ' <strong>' . implode(' ', $parts ) . '</strong>';
		}
	}

	return $title;
}


function dantes_add_widget_tabs( $tabs ) {
    $tabs[] = array(
        'title' => __( 'DANTES Widgets', 'dantes' ),
        'filter' => array(
            'groups' => array( 'dantes' )
        )
    );

    return $tabs;
}
add_filter( 'siteorigin_panels_widget_dialog_tabs', 'dantes_add_widget_tabs', 20 );


function dantes_add_widget_icons( $widgets ) {
	foreach ( $widgets as &$widget_data ) {
		if ( stripos( $widget_data[ 'title' ], 'dantes' ) !== false ) {
			$widget_data[ 'groups' ]	= array( 'dantes' );
		}
	}

    return $widgets;
}
add_filter( 'siteorigin_panels_widgets', 'dantes_add_widget_icons' );


function dantes_siteorigin_widgets_collection( $folders ) {
	$folders[] = get_template_directory() . '/widgets/';

	return $folders;
}
add_filter( 'siteorigin_widgets_widget_folders', 'dantes_siteorigin_widgets_collection' );


function dantes_bfa_icon_list( $icons ) {
	$dantes_icons		= array(
		'\e900'		=> 'icon-dantes-checkmark',
		'\e901'		=> 'icon-dantes-chat',
		'\e902'		=> 'icon-dantes-counselor',
		'\e903'		=> 'icon-dantes-leadership',
		'\e904'		=> 'icon-dantes-support',
		'\e905'		=> 'icon-dantes-virtual-ed-fairs',
		'\e906'		=> 'icon-dantes-chevron-thin-left',
		'\e907'		=> 'icon-dantes-chevron-thin-right'
	);

	$icons		= array_merge( $dantes_icons, $icons );

	return $icons;
}
add_filter( 'bfa_icon_list', 'dantes_bfa_icon_list', 20 );


function dantes_so_icon_families_filter( $icon_families ) {
    $icon_families[ 'dantes' ] = array(
        'name'		=> __( 'DANTES Icons', 'dantes' ),
        'style_uri'	=> get_stylesheet_directory_uri() . '/assets/fonts/icons/icons.css',
        'icons'		=> array(
			'icon-dantes-checkmark'	=> '&#xe900',
			'icon-dantes-chat' => '&#xe901',
			'icon-dantes-counselor' => '&#xe902',
			'icon-dantes-leadership' => '&#xe903',
			'icon-dantes-support' => '&#xe904',
			'icon-dantes-virtual-ed-fairs' => '&#xe905',
			'icon-dantes-chevron-thin-left' => '&#xe906',
			'icon-dantes-chevron-thin-right' => '&#xe907'
        )
    );

    return $icon_families;
}
add_filter( 'siteorigin_widgets_icon_families', 'dantes_so_icon_families_filter' );